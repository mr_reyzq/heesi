package id.esdm.recyclerproject.SlidingToolbar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import id.esdm.recyclerproject.R;


public class FragmentTree extends Fragment {
    public static FragmentOne newInstance(){
        return new FragmentOne();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tree, container, false);
        return rootView;
    }
}
