package id.esdm.recyclerproject.control;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.esdm.recyclerproject.R;
import id.esdm.recyclerproject.model.IdentitasModel;


/**
 * Created by ankit on 27/10/17.
 */

public class IdentitasAdapter extends RecyclerView.Adapter<IdentitasAdapter.ViewHolder> {

    private Context context;
    private List<IdentitasModel> list;

    public IdentitasAdapter(Context context, List<IdentitasModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.model_data_nama, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        IdentitasModel identitas = list.get(position);

        holder.txt1.setText(identitas.getTitle());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txt1;

        public ViewHolder(View itemView) {
            super(itemView);

            txt1 = itemView.findViewById(R.id.nama);
        }
    }

}