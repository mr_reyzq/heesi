package id.esdm.recyclerproject.SlidingToolbar;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import id.esdm.recyclerproject.R;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sliding_toolbar_main_activity);

        // initialization
        ViewPager viewPager = (ViewPager) findViewById(R.id.vp_scrolltab);
        TabLayout tab1 = (TabLayout) findViewById(R.id.tab1);

        MyAdapter adapter = new MyAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tab1.setupWithViewPager(viewPager);

    }

    class MyAdapter extends FragmentStatePagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment f = null;
            if (i == 0) {
                f = new FragmentOne();
            }
            if (i == 1) {
                f = new FragmentTwo();
            }


            return f;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            String name = null;

            if (position == 0) {
                name = "SATU";
            }

            if (position == 1) {
                name = "DUA";
            }

            return name;
        }
    }

}
