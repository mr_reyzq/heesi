package id.esdm.recyclerproject.model;

public class IdentitasModel {

    public String title;

    public IdentitasModel() {

    }

    public IdentitasModel(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
