<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_contact extends CI_Model {
	private $table_1 = 'contact';

	public function get_contact_user($userId='')
	{
		if ($userId) {
			$this->db->where('userId', $userId);
		}

		$this->db->select('*');
		$this->db->from($this->table_1);

		$data = $this->db->get();
		return $data->result();
	}

	public function get_detail_contact($contactId='')
	{
		if ($contactId) {
			$this->db->where('contactId', $contactId);
		}

		$this->db->select('*');
		$this->db->from($this->table_1);

		$data = $this->db->get();
		return $data->row();
	}

	public function insert_contact($data='')
	{
		$query = $this->db->insert($this->table_1, $data);

		if ($this->db->affected_rows() == 1) {
			return true;
		}else{
			return false;
		}
	}

	public function update_contact($contactId='', $data)
	{
		$this->db->where('contactId', $contactId);
		$query = $this->db->update($this->table_1,$data);

		if ($this->db->affected_rows() == 1) {
			return true;
		}else{
			return false;
		}
	}


	public function delete_contact($contactId='')
	{
		$this->db->where('contactId', $contactId);
		$query = $this->db->delete($this->table_1);

		if ($this->db->affected_rows() == 1) {
			return true;
		}else{
			return false;
		}
	}

	

}

/* End of file model_contact.php */
/* Location: ./application/models/model_contact.php */