<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_schedule extends CI_Model {
	private $table_1 = 'schedule';
	private $table_2 = 'reminder';

	public function get_schedule_user($userId='')
	{
		if ($userId) {
			$this->db->where('userId', $userId);
		}

		$this->db->select('*');
		$this->db->from($this->table_1);

		$data = $this->db->get();
		return $data->result();
	}

	public function get_detail_schedule($scheduleId='')
	{
		if ($scheduleId) {
			$this->db->where('scheduleId', $scheduleId);
		}

		$this->db->select('*');
		$this->db->from($this->table_1);

		$data = $this->db->get();
		return $data->row();
	}

	public function insert_schedule($data='')
	{
		$retr = array(
			'status' => false,
			'scheduleId' => ''
		);
		$query = $this->db->insert($this->table_1, $data);

		if ($this->db->affected_rows() == 1) {
			$retr['status'] = true;
			$retr['scheduleId'] = $this->db->insert_id();
		}

		return $retr;
	}

	public function update_schedule($scheduleId='', $data)
	{
		$this->db->where('scheduleId', $scheduleId);
		$query = $this->db->update($this->table_1,$data);

		if ($this->db->affected_rows() == 1) {
			return true;
		}else{
			return false;
		}
	}


	public function delete_schedule($scheduleId='')
	{
		$this->db->where('scheduleId', $scheduleId);
		$query = $this->db->delete($this->table_1);

		if ($this->db->affected_rows() == 1) {
			return true;
		}else{
			return false;
		}
	}


	public function get_reminder($scheduleId='')
	{
		$this->db->where('scheduleId', $scheduleId);
		$this->db->select('*');
		$data = $this->db->from($this->table_2);

		return $this->$this->db->get()->result();
	}

	public function create_reminder($data='')
	{
		$query = $this->db->insert($this->table_2, $data);

		if ($this->db->affected_rows() == 1) {
			return true;
		}else{
			return false;
		}
	}

	public function make_data_schedule($data='')
	{
		$retr = array();

		foreach ($data as $key => $value) {
			$value['reminder'] = $this->get_reminder($value->scheduleId);
		}
	}
	

}

/* End of file model_schedule.php */
/* Location: ./application/models/model_schedule.php */