<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_reminder extends CI_Model {
	private $table_1 = 'reminder';

	public function get_reminder_user($userId='')
	{
		if ($userId) {
			$this->db->where('userId', $userId);
		}

		$this->db->select('*');
		$this->db->from($this->table_1);

		$data = $this->db->get();
		return $data->result();
	}

	public function get_schedule_reminder($scheduleId='')
	{
		if ($scheduleId) {
			$this->db->where('scheduleId', $scheduleId);
		}

		$this->db->select('*');
		$this->db->from($this->table_1);

		$data = $this->db->get();
		return $data->row();
	}

	public function get_detail_reminder($reminderId='')
	{
		if ($reminderId) {
			$this->db->where('reminderId', $reminderId);
		}

		$this->db->select('*');
		$this->db->from($this->table_1);

		$data = $this->db->get();
		return $data->row();
	}

	public function insert_reminder($data='')
	{
		$query = $this->db->insert($this->table_1, $data);

		if ($this->db->affected_rows() == 1) {
			return true;
		}else{
			return false;
		}
	}

	public function update_reminder($reminderId='', $data)
	{
		$this->db->where('reminderId', $reminderId);
		$query = $this->db->update($this->table_1,$data);

		if ($this->db->affected_rows() == 1) {
			return true;
		}else{
			return false;
		}
	}


	public function delete_reminder($reminderId='')
	{
		$this->db->where('reminderId', $reminderId);
		$query = $this->db->delete($this->table_1);

		if ($this->db->affected_rows() == 1) {
			return true;
		}else{
			return false;
		}
	}

	

}

/* End of file model_reminder.php */
/* Location: ./application/models/model_reminder.php */