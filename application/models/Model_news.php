<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_news extends CI_Model {
	private $table_1 = 'news';

	public function get_news_user($userId='')
	{
		if ($userId) {
			$this->db->where('userId', $userId);
		}

		$this->db->select('*');
		$this->db->from($this->table_1);

		$data = $this->db->get();
		return $data->result();
	}

	public function get_detail_news($newsId='')
	{
		if ($newsId) {
			$this->db->where('newsId', $newsId);
		}

		$this->db->select('*');
		$this->db->from($this->table_1);

		$data = $this->db->get();
		return $data->row();
	}

	public function insert_news($data='')
	{
		$query = $this->db->insert($this->table_1, $data);

		if ($this->db->affected_rows() == 1) {
			return true;
		}else{
			return false;
		}
	}

	public function update_news($newsId='', $data)
	{
		$this->db->where('newsId', $newsId);
		$query = $this->db->update($this->table_1,$data);

		if ($this->db->affected_rows() == 1) {
			return true;
		}else{
			return false;
		}
	}


	public function delete_news($newsId='')
	{
		$this->db->where('newsId', $newsId);
		$query = $this->db->delete($this->table_1);

		if ($this->db->affected_rows() == 1) {
			return true;
		}else{
			return false;
		}
	}

	

}

/* End of file model_news.php */
/* Location: ./application/models/model_news.php */